package model;
 
/**
 * Lab 4 D0036D
 * @author Josefine Bexelius
 * 
 * Simple class to be able to save coordinates in pairs. 
 */
public class Coordinates {
	int x;
	int y;
	
	Coordinates(int x, int y){
		this.x = x;
		this.y = y;
	}
	
	public int getX(){
		return x;
	}
	
	public int getY(){
		return y;
	}

}


