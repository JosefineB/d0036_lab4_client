package model;

import java.util.ArrayList;
import java.util.Observable;

/**
 * Lab 4 D0036D
 * @author Josefine Bexelius
 * 
 * This class maintains the current state of the game.
 * Its observed by the game board, so it updates as the state changes.
 *
 */
public class GameState extends Observable {
	
	private ArrayList<Coordinates> playerList = new ArrayList<Coordinates>();
	private int clientId;
	private int currentPlayer;
	
	/**
	 * This is used in setup, when response the JOIN msg is received. 
	 * @param id : the id for this client
	 */
	public void setId(int id){
		this.clientId = id;
	}
	
	/**
	 * 
	 * @return the id of this client
	 */
	public int getId(){
		return clientId;
	}
	
	/**
	 * This is called when a newplayer msg arrives
	 * @param playerId : id of the current player
	 */
	public void addPlayer(int playerId){
		Coordinates player = new Coordinates(-1,-1);
		while(playerList.size() <= playerId){
			playerList.add(null);
		}
		playerList.set(playerId, player);
	}
	
	/**
	 * This is called when a newplayerposition msg arrives
	 * @param playerId
	 * @param x
	 * @param y
	 */
	public void movePlayer(int playerId, int x, int y){
		Coordinates player = new Coordinates(x, y);
		playerList.set(playerId, player);
		currentPlayer = playerId;
		setChanged();
		notifyObservers();
	}
	
	/**
	 * Called when playerleave msg arrives
	 * @param playerId
	 */
	public void deletePlayer(int playerId){
		playerList.set(playerId, null);
		currentPlayer = playerId;
		setChanged();
		notifyObservers();
	}
	
	/**
	 * The state of each connected player, where index
	 * represents playerId and each current x, y coordinates is saved in the list.
	 * @return list of current players positions
	 */
	public ArrayList<Coordinates> getPlayerList(){
		return playerList;
	}
	
	/**
	 * This s used to keep track of which player did the latest update	
	 * @return the id/index of the current player
	 */
	public int getCurrentPlayer(){
		return currentPlayer;
	}

}
