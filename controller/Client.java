package controller;

import java.io.*;
import java.net.*;

import model.GameState;

/*
 * Messages from client to server:
 * JOIN -> server responds by giving player id to client and broadcasting a NEWPLAYER message.
 * LEAVE -> server responds by closing socket and broadcasting a PLAYERLEAVE message.
 * MOVE-> server responds with a NEWPLAYERPOSITION message.
 */

/*
 * Messages from server to client:
 *  NEWPLAYER
 *  PLAYERLEAVE
 *  NEWPLAYERPOSITION
 */

/**
 * Lab 4 D0036D
 * @author Josefine Bexelius
 * This class is in charge of all the client server communication.
 * It gets its input from the graphical interface, sends it to the server,
 * processes the servers response and updates the current game state. 
 */
public class Client {
	
	private Socket socket; 
	private int port;
	private InetAddress addr;
	private BufferedReader input;
	private PrintWriter output;
	private GameState state;
	private int clientId;
	private int seqNr;
	
	/**
	 * 
	 * @param server : either name of address i.e. "localhost" or just the address in string format.
	 * @param port : the port the server listens on
	 */
	public Client(String server, int port, GameState state){
		try {
			this.addr = InetAddress.getByName(server);
		} catch (UnknownHostException e) {
			System.out.println(e.toString());
		}
		this.port = port;	
		this.state = state;
	}
	
	/**
	 * Setup of client socket and I/O streams.
	 */
	public void connectClient(){
		try{
			socket = new Socket(addr, port);
			output = new PrintWriter(socket.getOutputStream(), true);
			input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		}catch(IOException e){
			System.out.println(e.toString());
		}
	}
	
	/**
	 * Close client socket and I/O streams.
	 */
	public void disconnectClient(){
		try {
			input.close();
			socket.close();
		} catch (IOException e) {
			System.out.println(e.toString());
		}
		output.close();
		System.exit(0);
	}
	 /**
	  * 
	  * @param msg : type of input
	  * @param x : coordinates if move msg, else -1
	  * @param y : coordinates if move msg, else -1
	  * Sends client input to server.
	  */
	public void sendMsg(String msg, int x, int y){
		if(msg.equalsIgnoreCase("JOIN")){
			output.println("4:0:0:JOIN");
			output.flush();
			recieveMsg();
		}
		else if(msg.equalsIgnoreCase("MOVE")){
			seqNr++;
			//System.out.println("6:"+ seqNr +":"+ clientId + ":" + x + ":" + y +":MOVE");
			output.println("6:"+ clientId +":"+ seqNr + ":" + x + ":" + y +":MOVE");
			output.flush();
		}
		else if(msg.equalsIgnoreCase("LEAVE")){
			seqNr++;
			output.println("4:"+ clientId + ":" + seqNr + ":LEAVE");
			output.flush();
		}
	}
	
	/**
	 * Recieves server response and updates state.
	 */
	public void recieveMsg(){
		String reply;
		String[] msgData;
		try {
			while((reply = input.readLine()) != null){
				//System.out.println(reply);
				msgData = reply.split(":");
				if(msgData[3].equals("JOIN")){
					this.clientId = Integer.parseInt(msgData[1].trim());
					//Set this clients state id then add it to playerlist
					state.setId(clientId);
					state.addPlayer(clientId);
				}
				else if(msgData[3].equals("NEWPLAYER")){
					//add new player to playerlist
					int playerId = Integer.parseInt(msgData[1].trim());
					state.addPlayer(playerId);
				}
				else if (msgData[3].equals("PLAYERLEAVE")){
					//delete player from playerlist
					int playerId = Integer.parseInt(msgData[1].trim());
					if(playerId == clientId){
						disconnectClient();
					}else{
						state.deletePlayer(playerId);
					}
				}
				else if(msgData[5].equals("NEWPLAYERPOSITION")){
					//update player from playerlist with new coordinates
					int playerId = Integer.parseInt(msgData[1].trim());
					int x = Integer.parseInt(msgData[3].trim());
					int y = Integer.parseInt(msgData[4].trim());
					state.movePlayer(playerId, x, y);
				}
			}
		} catch (IOException e) {
			System.out.println(e.toString());
		}
	}
}
