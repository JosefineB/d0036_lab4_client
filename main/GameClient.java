package main;

import model.GameState;
import view.GameBoard;


/**
 * Lab 4 D0036D
 * @author Josefine Bexelius
 *
 */
public class GameClient {

	public static void main(String[] args) {
		GameState state = new GameState();
		GameBoard board = new GameBoard(500, state);
		state.addObserver(board);
		board.initGUI();

	}

}
