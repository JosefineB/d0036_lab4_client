package testing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

public class BasicServer {

	public static void main(String[] args) {
		
		try {
			ServerSocket socket = new ServerSocket(9999, 0, InetAddress.getByName("localhost"));
			System.out.println("Waiting for clients..."); // 1.Wait for client request
			Socket clientSocket = socket.accept(); // 2. Accept client.
			System.out.println("Client is connected."); // 3. Repeat process.
			BufferedReader input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			PrintWriter output  = new PrintWriter(clientSocket.getOutputStream(), true);
			String reply;
			while((reply = input.readLine()) != null){
				System.out.println(reply);
			}
			//output.println("4:0:1:JOIN");
			//String reply = input.readLine();
			//String[] data = reply.split(":");
			//output.println("6:0:1:NEWPLAYERPOSITION:" + data[4] + ":" + data[5]);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}
