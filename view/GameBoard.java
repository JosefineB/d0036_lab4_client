package view;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import javax.swing.*;

import controller.Client;
import model.Coordinates;
import model.GameState;

/**
 * Lab 4 D0036D
 * @author Josefine Bexelius
 * 
 * This class handles the graphical interface of the program.
 * It listens for input using actionlisteners and monitors for updates of the state
 * using observer/observable design pattern. 
 */
public class GameBoard implements Observer {
	
	private int size; 
	private GameState state; 
	private Client client; 
	private JFrame window;
	private CirclePanel panel;
	
	/**
	 * Takes the size of the gameboard, and instance of gamestate and
	 * creates an client instance. (Could possibly make it take address and port as args as well)
	 * @param size
	 * @param state
	 */
	public GameBoard(int size, GameState state){
		this.size = size;
		this.state = state;
		//client = new Client("localhost" , 9999);
		//client = new Client("10.0.2.15", 9997);
		client = new Client("130.240.40.7", 9998, state);
	}

	/**
	 * Sets up the GUI and creates of action listeners.
	 */
	public void initGUI(){
		//Draws the board and sends a JOIN message to the server
		client.connectClient();
		window = new JFrame("GameClient");
		window.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e){
				client.sendMsg("LEAVE", -1, -1);
				//Todo: need to fix disconnect as to not generate a broken pipe error serverside.
				//client.disconnectClient(); 
			}
		});
		panel = new CirclePanel(size);
		panel.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e){
				//-12 is just for centering circles being drawn.
				client.sendMsg("MOVE", e.getX()-12, e.getY()-12);
			}
		});
		window.setContentPane(panel);
		window.setSize(size, size);
		window.setVisible(true);
		window.setResizable(true);
		client.sendMsg("JOIN",-1 , -1);
		//window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
	}
	
	/**
	 * Runs when state is updated, redraws the contents of the game board.
	 */
	public void updateGUI(){
		ArrayList<Coordinates> playerList = state.getPlayerList();
		int playerId = state.getCurrentPlayer();
		if(playerList.get(playerId) != null){
			if(playerId == state.getId()){//playerId changes based on current player, result from getId is always this clients id
				panel.addCircle(new Circle(playerList.get(playerId).getX(), playerList.get(playerId).getY(), 24, Color.BLUE),state.getId()); 
			}
			else{
				panel.addCircle(new Circle(playerList.get(playerId).getX(), playerList.get(playerId).getY(), 24, Color.RED),playerId); 
			}
		}
		//A player deletion occured.
		else {
			panel.removeCircle(playerId);
		}
	}
	
	

	@Override
	public void update(Observable arg0, Object arg1) {
		updateGUI();	
	}

}
