package view;

import java.awt.Graphics;
import java.util.LinkedList;
import javax.swing.JPanel;

/**
 * Lab 4 D0036D
 * @author Josefine Bexelius
 * This class is the one painting the player components each time an update happens.
 */
public class CirclePanel extends JPanel {

	private LinkedList<Circle> circles = new LinkedList<Circle>();
	private int lenght;
	private int size;
	
	/**
	 * 
	 * @param size
	 */
	public CirclePanel(int size){
		this.size = size;
	}
	
	/**
	 * 
	 * @param circle
	 * @param index : current player  
	 * The index represents the playerId, this is why set() is used instead of add().
	 */
	public void addCircle(Circle circle, int index){
		lenght++;
		try{
			circles.set(index, circle);
		}catch (IndexOutOfBoundsException e){
			int i = circles.size();
			while(i <= index){
				circles.add(i, null);
				i++;
			}
			circles.set(index, circle); 
		}
		this.repaint(); 
	}
	
	/**
	 * Removes a player by setting its circle to null, meaning it wont be painted next update.
	 * @param index : current player
	 */
	public void removeCircle(int index){
		circles.set(index, null);
		this.repaint();
	}
	
	@Override
	public void paint(Graphics g) {
		g.clearRect(0, 0, size, size);
		for(Circle c: circles){ //Each circle in list is paint every time paint runs.
			if(c != null){
				c.draw(g);
			}
		}
	}
	
	public LinkedList<Circle> getCircles(){
		return circles;
	}
		
	public int getLenght(){
		return lenght;
	}
}
